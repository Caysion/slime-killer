if((keyboard_check(ord("Q")) || gamepad_button_check(0,gp_face2)) and !open){
	open = true;
	image_speed = 2;
	var amount = random(3);
	
	for(var i = 0; i <= amount+1;i++){
		var xRange = random_range(-15,15);
		var yRange = random_range(-15,15);
		var which = irandom_range(0,1);
		switch(which){
			case 0:{
				instance_create_layer(x+xRange,y+yRange,"LayerInteract",objHealthTonic);
				break;
			}
			case 1:{
				instance_create_layer(x+xRange,y+yRange,"LayerInteract",objStaminaPotion);
				break;
			}
		}
	}
	
	
}
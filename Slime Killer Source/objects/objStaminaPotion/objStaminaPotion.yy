{
    "id": "b91f67a3-eacb-42a6-bccc-4a8b20664853",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objStaminaPotion",
    "eventList": [
        {
            "id": "622dbc34-1ef1-4677-a0cf-1467c2fe63fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b91f67a3-eacb-42a6-bccc-4a8b20664853"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3bc8971d-e572-44d2-8614-a13de13533ab",
    "visible": true
}
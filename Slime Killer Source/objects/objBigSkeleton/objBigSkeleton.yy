{
    "id": "a3c0e199-7afb-4dac-bc87-0c54a1ea34f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBigSkeleton",
    "eventList": [
        {
            "id": "671dc4a7-bd5d-4bf5-b782-580d81ccfd29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "a3c0e199-7afb-4dac-bc87-0c54a1ea34f2"
        },
        {
            "id": "bb15e52c-bde5-40c6-be29-e41ff37e55e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a3c0e199-7afb-4dac-bc87-0c54a1ea34f2"
        },
        {
            "id": "2d269fd0-d48c-458e-9e8c-e8c6c7c1161e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a3c0e199-7afb-4dac-bc87-0c54a1ea34f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f56f6e57-243c-406e-9dd0-c0c70254ff48",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e4f6323a-2e9f-4160-82a3-918cca77eb36",
    "visible": true
}
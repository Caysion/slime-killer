{
    "id": "ce5caf7a-ab18-40c2-b2ae-b808cc77f9b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objWallDisappear",
    "eventList": [
        {
            "id": "fe27fe84-26c4-4b54-9045-3e2080b39ca3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ce5caf7a-ab18-40c2-b2ae-b808cc77f9b4"
        },
        {
            "id": "3154d768-0797-4d68-b21a-25911b7b923b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ce5caf7a-ab18-40c2-b2ae-b808cc77f9b4"
        }
    ],
    "maskSpriteId": "071e924d-9418-4a6b-ab70-991bc996fe95",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "071e924d-9418-4a6b-ab70-991bc996fe95",
    "visible": true
}
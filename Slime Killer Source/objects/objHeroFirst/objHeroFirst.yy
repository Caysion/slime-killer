{
    "id": "2529ff53-8d4a-4b20-8cac-a02aba5cfaaa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objHeroFirst",
    "eventList": [
        {
            "id": "18f98b00-974e-4679-8365-2f1628d2ebc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2529ff53-8d4a-4b20-8cac-a02aba5cfaaa"
        },
        {
            "id": "9821f374-aac8-4bea-84e2-5c900a34e233",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2529ff53-8d4a-4b20-8cac-a02aba5cfaaa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "62c2201a-43bb-45b2-8842-d86901a11513",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "9bed3228-3dec-4ae6-8417-1321e2773a2e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 15,
            "y": 0
        },
        {
            "id": "711aad1f-6617-41e7-ad4e-07f51e78331c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 15,
            "y": 20
        },
        {
            "id": "dff184e5-1983-4047-99df-9df8b275b4aa",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 20
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cce85c4f-0b90-4145-b17a-85d4db32c348",
    "visible": true
}
{
    "id": "f2e906eb-9ffd-4a41-8d69-8979b17662db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objEnemyProjectile",
    "eventList": [
        {
            "id": "8ff731c6-ff8b-4ad7-bed4-19a23658ca5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f2e906eb-9ffd-4a41-8d69-8979b17662db"
        },
        {
            "id": "b5457188-7e1d-4692-a6b6-fe3677bc0814",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f2e906eb-9ffd-4a41-8d69-8979b17662db"
        },
        {
            "id": "a6aedd83-4867-4351-88f9-a216c8bf13a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f2e906eb-9ffd-4a41-8d69-8979b17662db"
        },
        {
            "id": "e3475a07-063b-461c-a078-b733da6233ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f2e906eb-9ffd-4a41-8d69-8979b17662db"
        },
        {
            "id": "4009f971-1c27-4c03-901b-a2477d319b5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "73c0116f-92f3-498a-8f98-e50d1ea411fe",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f2e906eb-9ffd-4a41-8d69-8979b17662db"
        },
        {
            "id": "9c10dc9a-d4a2-47c3-afd1-3969535bf76c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "fd7d571c-d97e-4926-8dee-48a735a130d9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f2e906eb-9ffd-4a41-8d69-8979b17662db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cd989535-59b0-422e-a331-153c86aea992",
    "visible": true
}
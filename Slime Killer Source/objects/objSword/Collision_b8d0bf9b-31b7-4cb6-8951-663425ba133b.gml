var dmg = damage;

if(!instance_exists(objCrystal)){
	with(other){
		hp -= dmg;
	}
	audio_play_sound(sndSwordHit,0,0);
}else{
	audio_play_sound(sndDeflect,0,0);
}

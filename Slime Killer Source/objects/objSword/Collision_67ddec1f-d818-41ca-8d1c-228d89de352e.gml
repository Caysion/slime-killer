var dmg = damage;

with(other){
	hp -= dmg;
}
audio_play_sound(sndSwordHit,0,0);
damage = 0;

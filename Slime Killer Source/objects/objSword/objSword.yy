{
    "id": "fd7d571c-d97e-4926-8dee-48a735a130d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSword",
    "eventList": [
        {
            "id": "c3e16cc9-04ce-43ba-95ee-ddb83d47cedc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd7d571c-d97e-4926-8dee-48a735a130d9"
        },
        {
            "id": "2344b275-3561-4f0c-aeb6-eccb10a9c89a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fd7d571c-d97e-4926-8dee-48a735a130d9"
        },
        {
            "id": "71575786-ba85-4132-8654-fadb6c9ab284",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "fd7d571c-d97e-4926-8dee-48a735a130d9"
        },
        {
            "id": "67ddec1f-d818-41ca-8d1c-228d89de352e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f56f6e57-243c-406e-9dd0-c0c70254ff48",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fd7d571c-d97e-4926-8dee-48a735a130d9"
        },
        {
            "id": "b8d0bf9b-31b7-4cb6-8951-663425ba133b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "23584809-efcc-41f0-824d-2eb8a10fea72",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fd7d571c-d97e-4926-8dee-48a735a130d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
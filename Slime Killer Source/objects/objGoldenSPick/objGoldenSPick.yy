{
    "id": "9e351224-62be-4678-ba5a-857ab5441854",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objGoldenSPick",
    "eventList": [
        {
            "id": "8bd85d90-042d-4774-88d8-ebc2769f666a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9e351224-62be-4678-ba5a-857ab5441854"
        },
        {
            "id": "19956922-6b52-46f7-b72e-d57708f00305",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "9e351224-62be-4678-ba5a-857ab5441854"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "91031c02-9923-4a42-b5cc-5bc810685a55",
    "visible": true
}
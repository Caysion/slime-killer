{
    "id": "c29d29e7-5349-4f5e-a11d-0112d4944408",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objHeroGolden",
    "eventList": [
        {
            "id": "c2b2a065-80a2-459c-a30c-04eb8b044254",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c29d29e7-5349-4f5e-a11d-0112d4944408"
        },
        {
            "id": "4088021f-c164-4cc4-83b6-319946ed0951",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c29d29e7-5349-4f5e-a11d-0112d4944408"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "0d36e99e-159d-4ef5-b2b4-95a18eb419ad",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "66a15a7d-ef25-4d9d-8a4e-22284126e6e1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "1ea94352-4d7b-4d2b-a0d4-08c337317660",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 20
        },
        {
            "id": "d95209a2-2cdd-427a-8576-8faa09f918b6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 20
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "726a9e4a-2f5a-4e57-908e-732823c3e30d",
    "visible": true
}
{
    "id": "bfc59d21-fb05-43ea-a2ad-a75128261925",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMainWall",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "73c0116f-92f3-498a-8f98-e50d1ea411fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "071e924d-9418-4a6b-ab70-991bc996fe95",
    "visible": true
}
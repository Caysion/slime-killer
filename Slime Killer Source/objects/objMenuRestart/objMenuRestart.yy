{
    "id": "feedc39b-6a0a-4e88-88db-3e49971657b1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMenuRestart",
    "eventList": [
        {
            "id": "2087097f-b5f4-4f98-92b0-05f5b1f23af7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "feedc39b-6a0a-4e88-88db-3e49971657b1"
        },
        {
            "id": "35feaf64-ff81-47e5-a1ca-0e424be4ccb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "feedc39b-6a0a-4e88-88db-3e49971657b1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4911f5c3-0eec-41bf-96dc-4646149dc9e0",
    "visible": true
}
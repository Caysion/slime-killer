var minX = 16+11;
var minY = 16+14;
var maxX = room_width-22;
var maxY = room_height - 28;

var xRandom = irandom_range(minX,maxX);
var yRandom = irandom_range(minY,maxY);
var chance = irandom(3);

if(chance == 1) instance_create_layer(xRandom,yRandom,"LayerHero",objHealthTonic);
else instance_create_layer(xRandom,yRandom,"LayerHero",objFire);

fireAlarm = 30;
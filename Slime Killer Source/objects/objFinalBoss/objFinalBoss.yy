{
    "id": "23584809-efcc-41f0-824d-2eb8a10fea72",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFinalBoss",
    "eventList": [
        {
            "id": "e52e779a-9290-4cd1-ab74-f808cf478096",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "23584809-efcc-41f0-824d-2eb8a10fea72"
        },
        {
            "id": "7cea77db-b996-40f4-8f76-d0ca22172ed4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "23584809-efcc-41f0-824d-2eb8a10fea72"
        },
        {
            "id": "13d2cd13-3af1-4ac4-aaeb-cea537f5de88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "23584809-efcc-41f0-824d-2eb8a10fea72"
        },
        {
            "id": "ecf0f5d9-0338-4169-a98f-b0d11fd7750a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "23584809-efcc-41f0-824d-2eb8a10fea72"
        },
        {
            "id": "09f9c141-dba8-4625-840e-6d7851a62405",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "23584809-efcc-41f0-824d-2eb8a10fea72"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f56f6e57-243c-406e-9dd0-c0c70254ff48",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "f935e22a-9d19-4c97-8775-9d033c689db5",
    "visible": true
}
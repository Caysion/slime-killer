{
    "id": "6054ee3f-41ee-4bdb-990f-0b0dc88b4068",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSmallSlime",
    "eventList": [
        {
            "id": "b2b3caf5-b2e3-4893-8af8-2e671fcdd7b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6054ee3f-41ee-4bdb-990f-0b0dc88b4068"
        },
        {
            "id": "0169ad2c-70b7-41cd-a835-6b37571f05c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6054ee3f-41ee-4bdb-990f-0b0dc88b4068"
        },
        {
            "id": "62a1b1e0-1e2b-4ee9-817e-e05754a6bfa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6054ee3f-41ee-4bdb-990f-0b0dc88b4068"
        },
        {
            "id": "956fdfda-1ad2-4f54-b68a-824f59a26928",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "6054ee3f-41ee-4bdb-990f-0b0dc88b4068"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f56f6e57-243c-406e-9dd0-c0c70254ff48",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "44314df4-01ef-43fe-8192-e7f4dd5a886a",
    "visible": true
}
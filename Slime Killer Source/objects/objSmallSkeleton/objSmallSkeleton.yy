{
    "id": "e858ca1d-3b8f-4253-83c3-d811212fd50c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSmallSkeleton",
    "eventList": [
        {
            "id": "0cdaf30b-a700-4820-a31c-6fe5848677b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e858ca1d-3b8f-4253-83c3-d811212fd50c"
        },
        {
            "id": "3270ab35-06e6-4ceb-8798-1cf9e247f956",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e858ca1d-3b8f-4253-83c3-d811212fd50c"
        },
        {
            "id": "90d1d570-e257-4736-af7b-8250af2a7b31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e858ca1d-3b8f-4253-83c3-d811212fd50c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f56f6e57-243c-406e-9dd0-c0c70254ff48",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3bf38c14-d48e-4686-bc17-8dc85b6b8672",
    "visible": true
}
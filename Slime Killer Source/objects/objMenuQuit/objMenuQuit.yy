{
    "id": "23345200-61ad-42a9-944d-24e545554180",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMenuQuit",
    "eventList": [
        {
            "id": "16a10ff3-c118-4b7e-aacb-9908266d2314",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "23345200-61ad-42a9-944d-24e545554180"
        },
        {
            "id": "359db39a-d5ec-4d04-89db-972a25e8e586",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "23345200-61ad-42a9-944d-24e545554180"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a514498e-3e9f-4061-8736-e1de7a15a059",
    "visible": true
}
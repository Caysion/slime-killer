if(instance_exists(monsterList)){
	if(global.spawnMonsters){
		#region ifs
		if(room == rmFirst){
			instance_create_layer(215,130,"LayerEnemies",objSmallFlame);
			instance_create_layer(160,80,"LayerEnemies",objSmallFlame);
			instance_create_layer(160,190,"LayerEnemies",objSmallFlame);
		}
		if(room == rmSecond){
			instance_create_layer(130,130,"LayerEnemies",objBigFlame);
			instance_create_layer(160,64,"LayerEnemies",objSmallFlame);	
			instance_create_layer(130,180,"LayerEnemies",objSmallFlame);
			instance_create_layer(225,190,"LayerEnemies",objSmallFlame);
		}
		if(room == rmFourth){
			instance_create_layer(130,50,"LayerEnemies",objSmallSlime);
			instance_create_layer(130,223,"LayerEnemies",objBigSkeleton);	
			instance_create_layer(224,47,"LayerEnemies",objSmallFlame);
			instance_create_layer(224,224,"LayerEnemies",objSmallFlame);
		}
		if (room == rmFifth) {
		    instance_create_layer(210,210,"LayerEnemies",objBigSlime);
			instance_create_layer(224,80,"LayerEnemies",objBigSkeleton);	
			instance_create_layer(64,47,"LayerEnemies",objSmallFlame);
		}
		if (room == rmSeventh) {
		    instance_create_layer(132,50,"LayerEnemies",objSmallFlame);
			instance_create_layer(222,223,"LayerEnemies",objBigFlame);	
			instance_create_layer(208,64,"LayerEnemies",objBigSlime);
		}
		if (room == rmEigth) {
		    instance_create_layer(33,50,"LayerEnemies",objSmallSlime);
			instance_create_layer(223,225,"LayerEnemies",objSmallFlame);	
			instance_create_layer(223,50,"LayerEnemies",objBigSkeleton);
		}
		if (room == rmNineth) {
		    instance_create_layer(224,128,"LayerEnemies",objFinalBoss);	
			instance_create_layer(208,64,"LayerEnemies",objCrystal);
		}
		#endregion
	}
}
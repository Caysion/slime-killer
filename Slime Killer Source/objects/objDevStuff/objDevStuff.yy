{
    "id": "c17f9ef9-cd22-41e7-a208-f2cf90f06b95",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objDevStuff",
    "eventList": [
        {
            "id": "07245bcf-9b57-4fc6-80e7-9b9eb8699dd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c17f9ef9-cd22-41e7-a208-f2cf90f06b95"
        },
        {
            "id": "31050a6f-9829-42cb-ad9e-b4f481fa98f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 10,
            "m_owner": "c17f9ef9-cd22-41e7-a208-f2cf90f06b95"
        },
        {
            "id": "47f0a5db-f1f6-416c-aa5a-540362bd35fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c17f9ef9-cd22-41e7-a208-f2cf90f06b95"
        },
        {
            "id": "5a5671ab-8d69-4c97-a678-35401c6415d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c17f9ef9-cd22-41e7-a208-f2cf90f06b95"
        },
        {
            "id": "31843760-0c3e-4cbe-8e1f-4ef7566e0bb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "c17f9ef9-cd22-41e7-a208-f2cf90f06b95"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
{
    "id": "9e4d2935-c1f5-4a26-b2ff-230933d3161a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMenuResume",
    "eventList": [
        {
            "id": "a1bcbfe7-6cf2-4d29-8d0a-3d77610b1d50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9e4d2935-c1f5-4a26-b2ff-230933d3161a"
        },
        {
            "id": "879a7fcf-f91f-4703-a972-e93307d1ded6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "9e4d2935-c1f5-4a26-b2ff-230933d3161a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5de9ee03-a2f9-40e1-9d3a-fff06dc0e48d",
    "visible": true
}
{
    "id": "cf717300-6e7b-4867-85c0-383f3bb8d9a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBigFlame",
    "eventList": [
        {
            "id": "d10b172b-b8d7-4171-8965-628735d58715",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf717300-6e7b-4867-85c0-383f3bb8d9a1"
        },
        {
            "id": "1e7d1c25-d14e-433c-8689-6db1c5184e0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cf717300-6e7b-4867-85c0-383f3bb8d9a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f56f6e57-243c-406e-9dd0-c0c70254ff48",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7934d688-c1b8-4042-a41c-39ddf7f135c1",
    "visible": true
}
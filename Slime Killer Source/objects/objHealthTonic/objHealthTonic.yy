{
    "id": "82a50cf3-18f7-437b-8ffd-8736d4bc18e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objHealthTonic",
    "eventList": [
        {
            "id": "73a98c6b-c676-4f49-9232-7a653820cf2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "82a50cf3-18f7-437b-8ffd-8736d4bc18e5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f000d2b6-4c13-4384-8efa-0061d1830c75",
    "visible": true
}
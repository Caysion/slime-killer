spd = 2;
image_speed = 0;

maxHp = objDevStuff.devMaxHp;
hp = objDevStuff.devHp;

maxStamina = objDevStuff.devStam;
stamina = objDevStuff.devMaxStam;

global.looking = 1;
changeSprite = "";
thisSprite = sprite_index;
new = true;


alarm[0] = 10;
attackDelay = 0;

healthColor = c_green;
lowHealthColor = c_red;
poisonCounter = 0;
poisonState = false;

gamepad_set_axis_deadzone(0,0.2);
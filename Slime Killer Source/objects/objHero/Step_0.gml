var bbox_side;

key_down = 0;
key_left = 0;
key_right = 0;
key_up = 0;

if(gamepad_is_connected(0)){
	var xVal = gamepad_axis_value(0,gp_axislh);
	var yVal = gamepad_axis_value(0,gp_axislv);
	
	if(xVal > 0) key_right = xVal;	
	else key_left = -xVal;
	
	if(yVal > 0) key_down = yVal;
	else key_down = yVal;
}else{
	key_right = keyboard_check(ord("D"));
	key_left = keyboard_check(ord("A"));
	key_up = keyboard_check(ord("W"));
	key_down = keyboard_check(ord("S"));
}

hsp = (key_right - key_left) * spd;
vsp = (key_down - key_up) * spd;

if(hsp != 0 || vsp != 0){
	event_user(0);
}
else event_user(1);

//Horizontal Collision
if(hsp > 0){
	global.looking = 1;	
}
else if(hsp < 0){
	global.looking = -1;	
}
x += hsp;

y += vsp;


if(global.looking == -1 && new){
	sprite_index = changeSprite;
	new = false;
	
}

if(global.looking == 1 && !new){
	sprite_index = thisSprite;
	new = true;
}
if(hp <= 0) instance_destroy();

if(poisonState == true){
	if(poisonCounter % 20 == 0) hp-=0.5;
	poisonCounter++;	
}
if(keyboard_check(vk_space) || gamepad_button_check(0,gp_face1)) event_user(2);

attackDelay--;
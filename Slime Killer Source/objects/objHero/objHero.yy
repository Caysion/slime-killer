{
    "id": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objHero",
    "eventList": [
        {
            "id": "40a66201-7891-4f86-b222-3ae4027d5364",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a"
        },
        {
            "id": "8723863d-6fa8-41f6-80f3-f526460835a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a"
        },
        {
            "id": "b42435e1-bdf3-48d8-be73-f861756c41ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a"
        },
        {
            "id": "8d75a2cc-170b-4ba1-9ffe-2891da507909",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a"
        },
        {
            "id": "63f59ba9-15d0-44a1-9a63-1323b82508eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a"
        },
        {
            "id": "5cbb62da-8daa-468b-859e-512ea9ba9b23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a"
        },
        {
            "id": "412fe4d9-4413-49a9-ae78-5b3e111266d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a"
        },
        {
            "id": "e6806ae9-2dec-41cc-8f68-b80b6c9e09e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "e18be191-589f-4fdb-b224-093ce3cfba7c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "6d6eeb99-d3da-423c-a2ed-97eedcf61ffd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "5e463002-59fe-4277-ab4b-0e20db5ca191",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "2623b9f4-cd5b-4e1b-9a02-5e79df7047c1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
{
    "id": "841ca9a7-f5e7-40eb-9a34-9831bd2cdfdf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objGoldenSword",
    "eventList": [
        {
            "id": "8e0faf98-4a88-404c-8a87-b952f5778152",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "841ca9a7-f5e7-40eb-9a34-9831bd2cdfdf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fd7d571c-d97e-4926-8dee-48a735a130d9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9ad6f9a9-5787-4f1b-988b-6bc2cdca3375",
    "visible": true
}
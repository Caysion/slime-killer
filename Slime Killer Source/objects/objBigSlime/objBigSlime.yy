{
    "id": "8a31bb34-ff84-4620-bcb5-22ccaf815fde",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBigSlime",
    "eventList": [
        {
            "id": "b4c28615-ede9-48fe-8f0a-10cae0006189",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8a31bb34-ff84-4620-bcb5-22ccaf815fde"
        },
        {
            "id": "1c641499-6a5d-4e40-9f16-d16ff30dd123",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a31bb34-ff84-4620-bcb5-22ccaf815fde"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6054ee3f-41ee-4bdb-990f-0b0dc88b4068",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f2413b62-1f8d-4909-9beb-d9c23c8ded84",
    "visible": true
}
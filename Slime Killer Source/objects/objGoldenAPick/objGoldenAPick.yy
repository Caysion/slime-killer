{
    "id": "c661c843-2081-476f-8d44-6128391755b9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objGoldenAPick",
    "eventList": [
        {
            "id": "55afd219-7d6e-4d4f-9362-1968d57eb0bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c661c843-2081-476f-8d44-6128391755b9"
        },
        {
            "id": "7f3a3194-d789-4c00-9ead-3c7418dadf6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "c661c843-2081-476f-8d44-6128391755b9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "7f1dfa64-eb4c-46cf-b216-f99d605cc30a",
    "visible": true
}
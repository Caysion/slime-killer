{
    "id": "499292b1-4e21-4efa-bae8-f4173e4a9b0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objGrid",
    "eventList": [
        {
            "id": "0efed235-4a5e-4cc5-a89f-047a506779ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "499292b1-4e21-4efa-bae8-f4173e4a9b0f"
        },
        {
            "id": "3b5aff40-e00a-494f-8ceb-5c824c7fcaf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "499292b1-4e21-4efa-bae8-f4173e4a9b0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
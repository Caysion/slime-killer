var cellWidth = 16;
var cellHeight = 16;

var hcells = room_width div cellWidth;
var vcells = room_height div cellHeight;

global.grid = mp_grid_create(0,0,hcells,vcells,cellWidth,cellHeight);

mp_grid_add_instances(global.grid,objWall,0);
mp_grid_add_instances(global.grid,objTopWall,0);
mp_grid_add_instances(global.grid,objEnemy,1);
if(dealDamage){	
	var dmg = damage;
	with(other){
		hp -= dmg;
	}
	dealDamage = false;
	alarm[5] = 15;
	audio_play_sound(sndDamage,0,0);
}


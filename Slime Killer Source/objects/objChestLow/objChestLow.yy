{
    "id": "0fd3e1fc-17a7-4739-a747-f97be6f39895",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objChestLow",
    "eventList": [
        {
            "id": "60f65e4d-b0a0-4d4f-963e-33b6ffa97902",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0fd3e1fc-17a7-4739-a747-f97be6f39895"
        },
        {
            "id": "da9bbec3-825a-4fe3-a04e-535ab1ff34b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0fd3e1fc-17a7-4739-a747-f97be6f39895"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b48c69e9-9540-40fd-894c-ef21964a3dfe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "cd99c3c6-baf6-41f1-b9ce-671c52372193",
    "visible": true
}
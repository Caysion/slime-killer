{
    "id": "865eed13-7d8a-49b6-9d14-7bca64063dd1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTopWall",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "73c0116f-92f3-498a-8f98-e50d1ea411fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "232be79d-be5d-486a-a264-0764cc4fd8b1",
    "visible": true
}
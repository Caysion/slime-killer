{
    "id": "e505561d-1e47-4a49-802d-5c1169629832",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSmallFlame",
    "eventList": [
        {
            "id": "688a2af3-8fb7-421d-9a32-82aab1de025c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e505561d-1e47-4a49-802d-5c1169629832"
        },
        {
            "id": "17d4e471-b623-4d80-a771-0e36458e185a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e505561d-1e47-4a49-802d-5c1169629832"
        },
        {
            "id": "7026e80b-ebfb-4501-a0d8-5430f43e5562",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e505561d-1e47-4a49-802d-5c1169629832"
        },
        {
            "id": "b01a68a0-aec9-4fb0-b8e6-0691b4272663",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e505561d-1e47-4a49-802d-5c1169629832"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f56f6e57-243c-406e-9dd0-c0c70254ff48",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "977df634-e2de-4f8a-a109-7fb378a05841",
    "visible": true
}
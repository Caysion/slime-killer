{
    "id": "a339a3ac-ed7c-4d53-b584-41246f585275",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBronzeSword",
    "eventList": [
        {
            "id": "84dd0068-252f-4b28-873b-0a0bf4676bc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a339a3ac-ed7c-4d53-b584-41246f585275"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fd7d571c-d97e-4926-8dee-48a735a130d9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "44674cad-e43d-4533-93f6-847e56fc0666",
    "visible": true
}
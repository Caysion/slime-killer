{
    "id": "916a1fd5-f94a-40b6-83bb-1646f013ecd9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTeleport",
    "eventList": [
        {
            "id": "d67529b2-f2b4-446f-83ee-400099db5a50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18076606-d9f5-4cd3-a6b0-c86d75c6b08a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "916a1fd5-f94a-40b6-83bb-1646f013ecd9"
        }
    ],
    "maskSpriteId": "071e924d-9418-4a6b-ab70-991bc996fe95",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
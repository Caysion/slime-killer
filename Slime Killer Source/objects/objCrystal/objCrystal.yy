{
    "id": "b98460f6-42c5-4e09-be8c-b2e113c3decf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCrystal",
    "eventList": [
        {
            "id": "550efee2-9010-4e09-ac7d-8c4a1bc6bd43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b98460f6-42c5-4e09-be8c-b2e113c3decf"
        },
        {
            "id": "f45bf30e-d587-43ce-b3d9-87e5b1236f74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "b98460f6-42c5-4e09-be8c-b2e113c3decf"
        },
        {
            "id": "95d7cdbf-04aa-4af1-a376-51e4f3daac9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b98460f6-42c5-4e09-be8c-b2e113c3decf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f56f6e57-243c-406e-9dd0-c0c70254ff48",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
    "visible": true
}
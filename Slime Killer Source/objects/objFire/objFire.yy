{
    "id": "6dc0ed1a-3f83-4f21-a559-ac31119db910",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFire",
    "eventList": [
        {
            "id": "7626d1ba-c1f1-4e4d-8e02-b70f7bda31c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6dc0ed1a-3f83-4f21-a559-ac31119db910"
        },
        {
            "id": "90d949cd-7e40-44b3-b2bc-c398acd8e2ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6dc0ed1a-3f83-4f21-a559-ac31119db910"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
    "visible": true
}
{
    "id": "229fb943-1307-44ec-b6c4-90f3196a1019",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite38",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 271,
    "bbox_left": 1,
    "bbox_right": 546,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41ba6651-2762-4853-890d-cded8b958ea2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "229fb943-1307-44ec-b6c4-90f3196a1019",
            "compositeImage": {
                "id": "8bdc57d1-89ae-47d5-ba61-6b288323d0f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41ba6651-2762-4853-890d-cded8b958ea2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a722920-7ae3-4b6f-bcc5-b580d36d21a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41ba6651-2762-4853-890d-cded8b958ea2",
                    "LayerId": "4b954b20-0368-4739-af7a-3b19feba9a80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 272,
    "layers": [
        {
            "id": "4b954b20-0368-4739-af7a-3b19feba9a80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "229fb943-1307-44ec-b6c4-90f3196a1019",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 547,
    "xorig": 0,
    "yorig": 0
}
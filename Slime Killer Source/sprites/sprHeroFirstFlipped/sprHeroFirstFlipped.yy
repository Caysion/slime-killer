{
    "id": "4576e13a-728c-4b62-b297-10b878914297",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHeroFirstFlipped",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89db6328-9aa2-48e8-8bd7-d01e31a1d509",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4576e13a-728c-4b62-b297-10b878914297",
            "compositeImage": {
                "id": "8bbfaa1f-9eb9-46a1-b19a-99d185c545a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89db6328-9aa2-48e8-8bd7-d01e31a1d509",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ee6b18b-7a2a-48f2-be60-fbeb1307e806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89db6328-9aa2-48e8-8bd7-d01e31a1d509",
                    "LayerId": "64604876-c35c-44d8-b657-1197f1cde486"
                }
            ]
        },
        {
            "id": "503fdfbf-5ed0-4ccc-bdf7-df27e3f29ace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4576e13a-728c-4b62-b297-10b878914297",
            "compositeImage": {
                "id": "a1530894-ec81-4a05-9c69-065c5a0fdfb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "503fdfbf-5ed0-4ccc-bdf7-df27e3f29ace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72b7f57c-4058-4454-922e-803f33c1af8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "503fdfbf-5ed0-4ccc-bdf7-df27e3f29ace",
                    "LayerId": "64604876-c35c-44d8-b657-1197f1cde486"
                }
            ]
        },
        {
            "id": "129cc223-da12-4938-bf9d-b5d6f0d2aaf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4576e13a-728c-4b62-b297-10b878914297",
            "compositeImage": {
                "id": "84ecfa52-3eec-4001-ba6e-81b622d55e58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "129cc223-da12-4938-bf9d-b5d6f0d2aaf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa8f5f4-9e51-49a7-b040-7ba6e0e58d9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "129cc223-da12-4938-bf9d-b5d6f0d2aaf0",
                    "LayerId": "64604876-c35c-44d8-b657-1197f1cde486"
                }
            ]
        },
        {
            "id": "f0ea9fcd-656b-4f4f-b976-1db2cbb15597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4576e13a-728c-4b62-b297-10b878914297",
            "compositeImage": {
                "id": "62f03373-b3dd-4ff6-b069-917b59164b80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0ea9fcd-656b-4f4f-b976-1db2cbb15597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d380ed42-912e-4c56-a4f8-a369a581d152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0ea9fcd-656b-4f4f-b976-1db2cbb15597",
                    "LayerId": "64604876-c35c-44d8-b657-1197f1cde486"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "64604876-c35c-44d8-b657-1197f1cde486",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4576e13a-728c-4b62-b297-10b878914297",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 12,
    "yorig": 15
}
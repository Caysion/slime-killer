{
    "id": "21b5ccd1-23e5-49da-928b-9051d4bea073",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprChestLow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e75ac777-e50b-4480-930a-173424871dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b5ccd1-23e5-49da-928b-9051d4bea073",
            "compositeImage": {
                "id": "489f2cc2-c00a-4f13-a6e6-5f325e8e7249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e75ac777-e50b-4480-930a-173424871dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49fdea61-6dd4-437a-8ca7-4d99d1448d5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e75ac777-e50b-4480-930a-173424871dce",
                    "LayerId": "b2aea708-2ea8-41c0-a6da-b9883545c6a6"
                }
            ]
        },
        {
            "id": "af6f0938-cd4d-4264-bbd1-9705f05ef73c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b5ccd1-23e5-49da-928b-9051d4bea073",
            "compositeImage": {
                "id": "18ce9f0d-b361-4dce-80e2-2127bc949263",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af6f0938-cd4d-4264-bbd1-9705f05ef73c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c8b0a5a-e265-492d-a802-5a3d3990b466",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af6f0938-cd4d-4264-bbd1-9705f05ef73c",
                    "LayerId": "b2aea708-2ea8-41c0-a6da-b9883545c6a6"
                }
            ]
        },
        {
            "id": "da0b36f8-90b0-4a56-ab0b-cf9838cafc6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b5ccd1-23e5-49da-928b-9051d4bea073",
            "compositeImage": {
                "id": "41ad64ef-1094-4c6a-bd2f-bb985608d5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da0b36f8-90b0-4a56-ab0b-cf9838cafc6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0f6d4f3-01c5-4517-a460-5bc363250660",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da0b36f8-90b0-4a56-ab0b-cf9838cafc6b",
                    "LayerId": "b2aea708-2ea8-41c0-a6da-b9883545c6a6"
                }
            ]
        },
        {
            "id": "7e53994d-07cf-4c8d-8594-79d49bf194ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b5ccd1-23e5-49da-928b-9051d4bea073",
            "compositeImage": {
                "id": "80603174-a8c9-4c82-b049-36599d59b4ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e53994d-07cf-4c8d-8594-79d49bf194ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee098c54-f8f3-4319-bd42-2c748f80330d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e53994d-07cf-4c8d-8594-79d49bf194ed",
                    "LayerId": "b2aea708-2ea8-41c0-a6da-b9883545c6a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b2aea708-2ea8-41c0-a6da-b9883545c6a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21b5ccd1-23e5-49da-928b-9051d4bea073",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
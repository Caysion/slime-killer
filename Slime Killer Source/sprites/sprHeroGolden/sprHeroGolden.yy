{
    "id": "726a9e4a-2f5a-4e57-908e-732823c3e30d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHeroGolden",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7aacb0b-1569-41c3-948d-1e04c01c2029",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "726a9e4a-2f5a-4e57-908e-732823c3e30d",
            "compositeImage": {
                "id": "49692a49-3610-468f-889c-6be1d4ff04ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7aacb0b-1569-41c3-948d-1e04c01c2029",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a429119-6ab2-4b67-96ba-7b30f7a1916c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7aacb0b-1569-41c3-948d-1e04c01c2029",
                    "LayerId": "76ea796a-51a6-4fc1-a15c-70c6ebee1258"
                }
            ]
        },
        {
            "id": "7db0c616-17f9-49fc-8b86-335b0c674c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "726a9e4a-2f5a-4e57-908e-732823c3e30d",
            "compositeImage": {
                "id": "85cbb535-ec73-4ed0-be8b-81c0b74c2309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7db0c616-17f9-49fc-8b86-335b0c674c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c75cc92e-3008-4a57-b1ad-7b4f68e79478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7db0c616-17f9-49fc-8b86-335b0c674c87",
                    "LayerId": "76ea796a-51a6-4fc1-a15c-70c6ebee1258"
                }
            ]
        },
        {
            "id": "2e775db8-1db3-4fe7-853d-f5201e36a3e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "726a9e4a-2f5a-4e57-908e-732823c3e30d",
            "compositeImage": {
                "id": "20180343-e5cd-441d-827b-89e985089d28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e775db8-1db3-4fe7-853d-f5201e36a3e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6452e56-0331-4756-b8fa-e7423335d3a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e775db8-1db3-4fe7-853d-f5201e36a3e5",
                    "LayerId": "76ea796a-51a6-4fc1-a15c-70c6ebee1258"
                }
            ]
        },
        {
            "id": "24564808-1adf-4447-8158-44142f7f9a97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "726a9e4a-2f5a-4e57-908e-732823c3e30d",
            "compositeImage": {
                "id": "d7921363-71b0-4252-abe5-a867d93a8174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24564808-1adf-4447-8158-44142f7f9a97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00be695d-e700-466b-b73c-82889c5d4843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24564808-1adf-4447-8158-44142f7f9a97",
                    "LayerId": "76ea796a-51a6-4fc1-a15c-70c6ebee1258"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "76ea796a-51a6-4fc1-a15c-70c6ebee1258",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "726a9e4a-2f5a-4e57-908e-732823c3e30d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 12,
    "yorig": 15
}
{
    "id": "e6c57269-e779-42af-b09a-879e058e9b45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprB",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70eee76e-59b4-4e38-817a-7a82d89f3056",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6c57269-e779-42af-b09a-879e058e9b45",
            "compositeImage": {
                "id": "800455e9-97a5-4d37-9ed6-9dd0cf3c4496",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70eee76e-59b4-4e38-817a-7a82d89f3056",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a04f836-8de1-46bd-b056-66f1d9983163",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70eee76e-59b4-4e38-817a-7a82d89f3056",
                    "LayerId": "1ee3aeb4-9648-42e9-ad5a-edb2fa45407b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1ee3aeb4-9648-42e9-ad5a-edb2fa45407b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6c57269-e779-42af-b09a-879e058e9b45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
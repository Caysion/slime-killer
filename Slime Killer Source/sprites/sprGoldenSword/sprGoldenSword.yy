{
    "id": "9ad6f9a9-5787-4f1b-988b-6bc2cdca3375",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGoldenSword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d086090-5f06-4383-9438-32795d9a441b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ad6f9a9-5787-4f1b-988b-6bc2cdca3375",
            "compositeImage": {
                "id": "df0b52e3-0bd4-4791-96b7-266e493a92cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d086090-5f06-4383-9438-32795d9a441b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91abe08e-a241-4acc-8022-b1285151cc51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d086090-5f06-4383-9438-32795d9a441b",
                    "LayerId": "5b4f90fb-73f6-4bc7-b304-679dba604fb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "5b4f90fb-73f6-4bc7-b304-679dba604fb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ad6f9a9-5787-4f1b-988b-6bc2cdca3375",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 2
}
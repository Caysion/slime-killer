{
    "id": "c1e97bb8-3939-4b94-9ed2-3b81b368159b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSpace",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16f3345a-9f81-49d3-8ddc-80138ddfb50b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1e97bb8-3939-4b94-9ed2-3b81b368159b",
            "compositeImage": {
                "id": "25806fa3-2d57-4728-81e4-719a779328dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16f3345a-9f81-49d3-8ddc-80138ddfb50b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57f66f54-75b0-4ad8-a178-d3f9689419c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16f3345a-9f81-49d3-8ddc-80138ddfb50b",
                    "LayerId": "118ea6f8-1866-4793-822a-ecfccd2e47c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "118ea6f8-1866-4793-822a-ecfccd2e47c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1e97bb8-3939-4b94-9ed2-3b81b368159b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
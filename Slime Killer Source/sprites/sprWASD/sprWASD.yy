{
    "id": "7ced33a5-0dc7-471f-b1e0-09858913862c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWASD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b92a83cd-714e-4631-aede-80f52254ffab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ced33a5-0dc7-471f-b1e0-09858913862c",
            "compositeImage": {
                "id": "da5122bf-8db7-4e12-851a-32b30aed2f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b92a83cd-714e-4631-aede-80f52254ffab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67624f35-4a94-4eff-8845-6d4e7fe4b330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b92a83cd-714e-4631-aede-80f52254ffab",
                    "LayerId": "2ce796c3-79eb-4332-8403-d98ea9e6f7ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "2ce796c3-79eb-4332-8403-d98ea9e6f7ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ced33a5-0dc7-471f-b1e0-09858913862c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 0,
    "yorig": 0
}
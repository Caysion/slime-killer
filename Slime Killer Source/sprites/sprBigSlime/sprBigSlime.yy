{
    "id": "f2413b62-1f8d-4909-9beb-d9c23c8ded84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBigSlime",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbf984d3-4cc9-4c58-8fff-4eccbb4be281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2413b62-1f8d-4909-9beb-d9c23c8ded84",
            "compositeImage": {
                "id": "859fc36f-ae24-447c-a1c1-c2af67e80ad4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbf984d3-4cc9-4c58-8fff-4eccbb4be281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87b1b429-ba0d-415f-8c0f-552661c7e932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf984d3-4cc9-4c58-8fff-4eccbb4be281",
                    "LayerId": "20f7c758-0758-47fd-af9b-1291a8f2c75f"
                }
            ]
        },
        {
            "id": "d800c4db-34d8-4fea-96c9-d12603b454b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2413b62-1f8d-4909-9beb-d9c23c8ded84",
            "compositeImage": {
                "id": "cb1c398d-6da0-46c0-a5a9-4ba267db536a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d800c4db-34d8-4fea-96c9-d12603b454b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fbd555b-6707-4314-8cee-0376dee7fcbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d800c4db-34d8-4fea-96c9-d12603b454b2",
                    "LayerId": "20f7c758-0758-47fd-af9b-1291a8f2c75f"
                }
            ]
        },
        {
            "id": "f86a2c86-efc0-4227-a50f-71d57b7ee6f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2413b62-1f8d-4909-9beb-d9c23c8ded84",
            "compositeImage": {
                "id": "e76e5cf3-7d6e-411f-9526-b08891e4f272",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f86a2c86-efc0-4227-a50f-71d57b7ee6f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08b1b055-aba3-45e0-bde9-75f32c9adae5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f86a2c86-efc0-4227-a50f-71d57b7ee6f2",
                    "LayerId": "20f7c758-0758-47fd-af9b-1291a8f2c75f"
                }
            ]
        },
        {
            "id": "2234017b-7ec6-4fab-9ffb-dca6ff1990e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2413b62-1f8d-4909-9beb-d9c23c8ded84",
            "compositeImage": {
                "id": "2fd6dc24-9ec7-41b1-9223-4f2445543bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2234017b-7ec6-4fab-9ffb-dca6ff1990e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ef43fe9-4a59-4cf7-9f75-0a9e67bb84bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2234017b-7ec6-4fab-9ffb-dca6ff1990e8",
                    "LayerId": "20f7c758-0758-47fd-af9b-1291a8f2c75f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "20f7c758-0758-47fd-af9b-1291a8f2c75f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2413b62-1f8d-4909-9beb-d9c23c8ded84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 0
}
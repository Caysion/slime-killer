{
    "id": "81fbdf86-6111-48ac-adff-3e02eb2f798f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTilesetBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 0,
    "bbox_right": 207,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b86c656b-4da8-44c2-8178-3e85abb5a080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81fbdf86-6111-48ac-adff-3e02eb2f798f",
            "compositeImage": {
                "id": "7c40009f-6cfb-4c8b-adea-bbbcc4c22450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b86c656b-4da8-44c2-8178-3e85abb5a080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "559e02ba-60eb-432c-a7e1-218f8f3c7643",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b86c656b-4da8-44c2-8178-3e85abb5a080",
                    "LayerId": "cbe4dc87-7a70-4ecf-8b18-f77e01f1353b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "cbe4dc87-7a70-4ecf-8b18-f77e01f1353b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81fbdf86-6111-48ac-adff-3e02eb2f798f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 208,
    "xorig": 0,
    "yorig": 0
}
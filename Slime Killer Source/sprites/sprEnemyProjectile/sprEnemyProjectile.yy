{
    "id": "cd989535-59b0-422e-a331-153c86aea992",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprEnemyProjectile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91b33529-1f0a-4070-8615-6196dc1f99e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd989535-59b0-422e-a331-153c86aea992",
            "compositeImage": {
                "id": "d10ab159-f1e9-45be-8143-1c3b54a1e263",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91b33529-1f0a-4070-8615-6196dc1f99e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4bbbb4a-e44e-409e-b0a4-2412936b88b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91b33529-1f0a-4070-8615-6196dc1f99e8",
                    "LayerId": "c26d469f-f4f4-4305-a2fe-a4f9af62bc1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "c26d469f-f4f4-4305-a2fe-a4f9af62bc1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd989535-59b0-422e-a331-153c86aea992",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 2
}
{
    "id": "5de9ee03-a2f9-40e1-9d3a-fff06dc0e48d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenuResume",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f2d46bb-59bf-46af-a19c-cda4690e527e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5de9ee03-a2f9-40e1-9d3a-fff06dc0e48d",
            "compositeImage": {
                "id": "dd7adbc5-e388-4b5e-bb25-f8ad4e5aeaac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f2d46bb-59bf-46af-a19c-cda4690e527e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbdd782b-fe03-432c-8c62-26b3605472f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f2d46bb-59bf-46af-a19c-cda4690e527e",
                    "LayerId": "a071de82-d37c-4f1a-81fe-9cb478ecb1b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a071de82-d37c-4f1a-81fe-9cb478ecb1b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5de9ee03-a2f9-40e1-9d3a-fff06dc0e48d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}
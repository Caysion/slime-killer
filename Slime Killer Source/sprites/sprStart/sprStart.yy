{
    "id": "bb9f99e2-66f1-4088-aa38-82eb66a2cd0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24ffb477-4af8-4c5e-8fce-e20048357bfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb9f99e2-66f1-4088-aa38-82eb66a2cd0e",
            "compositeImage": {
                "id": "d568ad8f-c0b8-4ac0-a92a-da01f732be68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24ffb477-4af8-4c5e-8fce-e20048357bfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eea087f1-0997-4c1f-9b5b-3f6ea84cf08f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24ffb477-4af8-4c5e-8fce-e20048357bfa",
                    "LayerId": "813c8757-1732-4b94-a8ca-616a724080cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "813c8757-1732-4b94-a8ca-616a724080cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb9f99e2-66f1-4088-aa38-82eb66a2cd0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
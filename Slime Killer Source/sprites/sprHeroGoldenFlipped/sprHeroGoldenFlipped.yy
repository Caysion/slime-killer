{
    "id": "8ac5fe90-adcc-4f07-9a26-5d0e2a8b73c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHeroGoldenFlipped",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e38d06e-aae7-4bb4-a4eb-ad89468d8b90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ac5fe90-adcc-4f07-9a26-5d0e2a8b73c6",
            "compositeImage": {
                "id": "ef0c3099-8181-4403-95a0-db36d117b782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e38d06e-aae7-4bb4-a4eb-ad89468d8b90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41783dde-8dec-4d93-b89d-1515fa4dc720",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e38d06e-aae7-4bb4-a4eb-ad89468d8b90",
                    "LayerId": "817c6d05-825f-4748-85ee-5e8c6f012f50"
                }
            ]
        },
        {
            "id": "96f6ab29-c4f4-4950-987b-07168a48239e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ac5fe90-adcc-4f07-9a26-5d0e2a8b73c6",
            "compositeImage": {
                "id": "6dc18117-c920-4647-a21f-82fedcf09ef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96f6ab29-c4f4-4950-987b-07168a48239e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "125ccbc4-37c5-447a-beae-9305d16657d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96f6ab29-c4f4-4950-987b-07168a48239e",
                    "LayerId": "817c6d05-825f-4748-85ee-5e8c6f012f50"
                }
            ]
        },
        {
            "id": "64927d6c-1203-4f44-9cf5-a3906ec7fd91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ac5fe90-adcc-4f07-9a26-5d0e2a8b73c6",
            "compositeImage": {
                "id": "6918a94e-869b-4745-b465-1f95fe68580b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64927d6c-1203-4f44-9cf5-a3906ec7fd91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5cfd916-310b-48cf-9b76-96f08d6aa468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64927d6c-1203-4f44-9cf5-a3906ec7fd91",
                    "LayerId": "817c6d05-825f-4748-85ee-5e8c6f012f50"
                }
            ]
        },
        {
            "id": "cabfa691-22e8-4621-b321-40b75f62142c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ac5fe90-adcc-4f07-9a26-5d0e2a8b73c6",
            "compositeImage": {
                "id": "b6c1e215-441d-4cf5-b9f8-90bb99b7ed62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cabfa691-22e8-4621-b321-40b75f62142c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "598c28b9-8ad0-4f90-808a-bd8b5ad469ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cabfa691-22e8-4621-b321-40b75f62142c",
                    "LayerId": "817c6d05-825f-4748-85ee-5e8c6f012f50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "817c6d05-825f-4748-85ee-5e8c6f012f50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ac5fe90-adcc-4f07-9a26-5d0e2a8b73c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 12,
    "yorig": 15
}
{
    "id": "44674cad-e43d-4533-93f6-847e56fc0666",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBronzeSword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "179876b1-686a-40ed-b4cc-8d7746cffcb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44674cad-e43d-4533-93f6-847e56fc0666",
            "compositeImage": {
                "id": "fbfa98c2-b532-497d-8813-7608797c50a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "179876b1-686a-40ed-b4cc-8d7746cffcb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4053f6d2-3162-47dd-b47b-4009b600bb95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "179876b1-686a-40ed-b4cc-8d7746cffcb5",
                    "LayerId": "2cdf927c-5c74-4056-bed2-744ddea193ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "2cdf927c-5c74-4056-bed2-744ddea193ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44674cad-e43d-4533-93f6-847e56fc0666",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 2
}
{
    "id": "cd99c3c6-baf6-41f1-b9ce-671c52372193",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprChestLowNo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2789662f-bf8e-4967-8358-8ed045b28aca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd99c3c6-baf6-41f1-b9ce-671c52372193",
            "compositeImage": {
                "id": "0b9a3faf-2c0c-484d-aa60-3b790f24651e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2789662f-bf8e-4967-8358-8ed045b28aca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3f82fe5-2e7f-48b8-97b8-fad44487c95e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2789662f-bf8e-4967-8358-8ed045b28aca",
                    "LayerId": "9c7aa417-a5ba-4824-b43b-9baa299c4d03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9c7aa417-a5ba-4824-b43b-9baa299c4d03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd99c3c6-baf6-41f1-b9ce-671c52372193",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
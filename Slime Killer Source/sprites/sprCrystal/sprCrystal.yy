{
    "id": "d35ee104-60d1-4368-a185-4f1f6751e22f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCrystal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa1cf32d-d775-453d-85cb-1d4f84879cec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "e3038780-f6ea-4e2c-a93c-0177d82e31d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa1cf32d-d775-453d-85cb-1d4f84879cec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea93c5d0-cc1f-4b2f-9e77-2e64f6f67321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa1cf32d-d775-453d-85cb-1d4f84879cec",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "48bc110c-df1a-4f8d-a531-7de46978a6f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "a46df49b-e03e-4276-a024-673e7802d2dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48bc110c-df1a-4f8d-a531-7de46978a6f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71645dc2-fd12-4c72-895f-d86355aee237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48bc110c-df1a-4f8d-a531-7de46978a6f7",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "1b25361b-3e26-4541-a811-f01055d40151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "0ef67137-1833-45e8-b5d6-5c12a6a8a7dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b25361b-3e26-4541-a811-f01055d40151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0346084b-106c-450f-8d5e-00b268df3555",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b25361b-3e26-4541-a811-f01055d40151",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "4c3d4547-4683-4778-ba22-9aa1ec825b77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "f3058709-2bce-4b51-8227-4d745d43ea8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c3d4547-4683-4778-ba22-9aa1ec825b77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db9b50c9-c843-4759-b4ab-8d1553d6a34b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c3d4547-4683-4778-ba22-9aa1ec825b77",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "d70d4706-2b59-4bbf-9660-5cbb452c2864",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "f89245ef-035b-4c52-ba41-3679b44b214b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d70d4706-2b59-4bbf-9660-5cbb452c2864",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88ca71ba-be32-4884-ad5b-d17c6248d508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d70d4706-2b59-4bbf-9660-5cbb452c2864",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "04135ab4-91bf-4956-af23-c923612413df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "fd2b1523-8edc-4d36-b2b3-15d705b6370a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04135ab4-91bf-4956-af23-c923612413df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8989c014-a65f-4f52-a8a4-5c9e87619e3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04135ab4-91bf-4956-af23-c923612413df",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "09c6d7ba-8b02-4217-acc0-1b0aeccac37d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "280fd5e9-a418-4712-bbc7-041e118934c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09c6d7ba-8b02-4217-acc0-1b0aeccac37d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63130e1d-405e-4ce2-ae66-da2f6dcc7a73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09c6d7ba-8b02-4217-acc0-1b0aeccac37d",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "8d30d9ed-22a2-4e22-8322-fea558506611",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "09444fae-cffd-4d0c-82ac-a986019f48e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d30d9ed-22a2-4e22-8322-fea558506611",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0fdc298-9f2a-46d6-a3e1-88a6083e094a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d30d9ed-22a2-4e22-8322-fea558506611",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "c393f4ed-d23e-43a8-ad66-6c9723d9cc33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "757ad393-c70a-4b7f-800a-d60ed5842954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c393f4ed-d23e-43a8-ad66-6c9723d9cc33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd15cb1d-b938-47ef-bb84-e847f9298606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c393f4ed-d23e-43a8-ad66-6c9723d9cc33",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "45ba7a00-51e9-411a-b53e-d1c44f10b07e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "ad304696-cdea-42bb-8bf2-aa1be0321215",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45ba7a00-51e9-411a-b53e-d1c44f10b07e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f51730c4-b19a-4017-aec8-815dc5fea1d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45ba7a00-51e9-411a-b53e-d1c44f10b07e",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "2bde539d-8162-491b-a2ba-6ec6f158c6f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "0ee58e28-b916-4ae4-852c-42816088cbc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bde539d-8162-491b-a2ba-6ec6f158c6f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0987d21-2a90-460b-b893-0c2838e86fc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bde539d-8162-491b-a2ba-6ec6f158c6f6",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "b1e2925c-592e-44d3-be68-8a62b0e9b317",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "d6ec6796-78a3-4a57-9eb7-a8e185db68d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1e2925c-592e-44d3-be68-8a62b0e9b317",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85165c16-b80a-47e1-a359-f68cd9421c75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1e2925c-592e-44d3-be68-8a62b0e9b317",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "57f57cae-a003-4599-9fa0-3f405388855b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "ce281b0e-2456-4979-aaef-0d8541cdb23a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57f57cae-a003-4599-9fa0-3f405388855b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcd76560-3469-4c2e-b7c2-915573aa5efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57f57cae-a003-4599-9fa0-3f405388855b",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "8ce2138e-c228-4154-86d4-0b9555e5bccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "ab78a208-e38a-4ab4-bc3e-ff1b36d6d421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce2138e-c228-4154-86d4-0b9555e5bccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "389db704-1b41-477e-8b0b-521e1c444eaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce2138e-c228-4154-86d4-0b9555e5bccf",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "08ce2c38-b1c9-4494-8638-43062cd5c26b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "bcc64b58-bafd-4218-affe-453e31e2b16d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ce2c38-b1c9-4494-8638-43062cd5c26b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5124f1b6-7a00-4c94-9a77-cfbe9fff3ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ce2c38-b1c9-4494-8638-43062cd5c26b",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        },
        {
            "id": "c40fac34-a832-4292-af60-4f37a5c8d4d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "compositeImage": {
                "id": "eee0f58b-bc27-46c3-938a-f69ff5be1739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c40fac34-a832-4292-af60-4f37a5c8d4d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb5eecbf-f11a-4bd5-a2b1-646b2b7c8e5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c40fac34-a832-4292-af60-4f37a5c8d4d1",
                    "LayerId": "89bab441-7740-4f19-a72f-876d0e399391"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "89bab441-7740-4f19-a72f-876d0e399391",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d35ee104-60d1-4368-a185-4f1f6751e22f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
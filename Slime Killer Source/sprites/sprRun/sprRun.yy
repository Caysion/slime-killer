{
    "id": "6b5f1193-6bfa-4bba-8d53-3101cda50037",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16c18487-6c17-4dae-9f33-b9af8924588c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5f1193-6bfa-4bba-8d53-3101cda50037",
            "compositeImage": {
                "id": "b2a05f75-4970-48ad-a5af-fa07a764ca5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16c18487-6c17-4dae-9f33-b9af8924588c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39b5d3d2-7eb2-4cfd-b531-18b849fe3ac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16c18487-6c17-4dae-9f33-b9af8924588c",
                    "LayerId": "cf0f28a4-89a0-4242-ad0a-14344517cb29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "cf0f28a4-89a0-4242-ad0a-14344517cb29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b5f1193-6bfa-4bba-8d53-3101cda50037",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "117e31be-f0cc-4b46-aee5-f1340812de16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSlab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9cae313-8c22-4012-8f73-bf1ce00ad017",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "117e31be-f0cc-4b46-aee5-f1340812de16",
            "compositeImage": {
                "id": "068b1652-35b6-41d6-8429-d7f96ad3ef5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9cae313-8c22-4012-8f73-bf1ce00ad017",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ec454f-29a0-4ca1-bce0-6c1b4be32de0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9cae313-8c22-4012-8f73-bf1ce00ad017",
                    "LayerId": "8e059a41-f8e8-401d-8e3e-fdc4e6163a16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8e059a41-f8e8-401d-8e3e-fdc4e6163a16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "117e31be-f0cc-4b46-aee5-f1340812de16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
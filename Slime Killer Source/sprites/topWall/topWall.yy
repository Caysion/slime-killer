{
    "id": "232be79d-be5d-486a-a264-0764cc4fd8b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "topWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "361395eb-8084-4380-b768-0503a9ad24f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "232be79d-be5d-486a-a264-0764cc4fd8b1",
            "compositeImage": {
                "id": "29783b17-7c38-48e5-a402-1af4a966b495",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "361395eb-8084-4380-b768-0503a9ad24f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4acf65dd-ba4d-4ec7-84dd-f455c603027f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "361395eb-8084-4380-b768-0503a9ad24f9",
                    "LayerId": "2e6e3209-6e0e-4e03-929b-7619c00715b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2e6e3209-6e0e-4e03-929b-7619c00715b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "232be79d-be5d-486a-a264-0764cc4fd8b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
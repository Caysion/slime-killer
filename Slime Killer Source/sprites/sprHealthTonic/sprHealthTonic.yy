{
    "id": "f000d2b6-4c13-4384-8efa-0061d1830c75",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHealthTonic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8989dc2b-cb7e-4166-ae13-6dd3670e47ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f000d2b6-4c13-4384-8efa-0061d1830c75",
            "compositeImage": {
                "id": "0087fd1e-ea81-41e0-b72b-3e88c66e5a43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8989dc2b-cb7e-4166-ae13-6dd3670e47ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e87e5037-5609-449a-a1e5-d50ae9c0eefd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8989dc2b-cb7e-4166-ae13-6dd3670e47ee",
                    "LayerId": "29605507-918a-4a46-87dd-26d76e1edf56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "29605507-918a-4a46-87dd-26d76e1edf56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f000d2b6-4c13-4384-8efa-0061d1830c75",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
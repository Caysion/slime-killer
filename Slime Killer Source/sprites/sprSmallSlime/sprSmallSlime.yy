{
    "id": "44314df4-01ef-43fe-8192-e7f4dd5a886a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSmallSlime",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68f54ceb-12d9-45cc-8f17-7af0ffda6356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44314df4-01ef-43fe-8192-e7f4dd5a886a",
            "compositeImage": {
                "id": "5883463b-3d5a-4fcd-ac08-e1b022c6670a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68f54ceb-12d9-45cc-8f17-7af0ffda6356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0290ae6f-ca36-4f82-bb70-90e17293ea3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68f54ceb-12d9-45cc-8f17-7af0ffda6356",
                    "LayerId": "4621ff7b-b41e-4395-8cda-20cf4963cf9c"
                }
            ]
        },
        {
            "id": "71ccd197-544a-40c4-aeb0-40e6c4b60e0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44314df4-01ef-43fe-8192-e7f4dd5a886a",
            "compositeImage": {
                "id": "bdbb25ee-ed65-4cfc-a0a6-ca4566de1e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71ccd197-544a-40c4-aeb0-40e6c4b60e0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e3118ff-ca7b-4444-90a7-3fb9d9055283",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71ccd197-544a-40c4-aeb0-40e6c4b60e0a",
                    "LayerId": "4621ff7b-b41e-4395-8cda-20cf4963cf9c"
                }
            ]
        },
        {
            "id": "2039e250-5fac-4936-b059-0087648e7675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44314df4-01ef-43fe-8192-e7f4dd5a886a",
            "compositeImage": {
                "id": "c2bf6a8f-a0e9-4322-96e8-a6381269cf83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2039e250-5fac-4936-b059-0087648e7675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd85d51-f36e-4d25-89ad-c8af48819d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2039e250-5fac-4936-b059-0087648e7675",
                    "LayerId": "4621ff7b-b41e-4395-8cda-20cf4963cf9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "4621ff7b-b41e-4395-8cda-20cf4963cf9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44314df4-01ef-43fe-8192-e7f4dd5a886a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 0
}
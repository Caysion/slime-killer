{
    "id": "f935e22a-9d19-4c97-8775-9d033c689db5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFinalBoss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18d2d7be-6879-4e55-8a92-bd2e1602385c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f935e22a-9d19-4c97-8775-9d033c689db5",
            "compositeImage": {
                "id": "3eebcff2-7a94-4351-afed-9c06169d0cc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18d2d7be-6879-4e55-8a92-bd2e1602385c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71700d6c-ef9c-4672-bc96-6aa1c6713adf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18d2d7be-6879-4e55-8a92-bd2e1602385c",
                    "LayerId": "a0dd1581-e4fd-45d3-ab47-19c1a0d8d146"
                }
            ]
        },
        {
            "id": "aac59a3f-2b82-43db-9f1d-b7c2af7e1fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f935e22a-9d19-4c97-8775-9d033c689db5",
            "compositeImage": {
                "id": "41ef1aaa-8a15-4fe8-983b-2efffe785b59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aac59a3f-2b82-43db-9f1d-b7c2af7e1fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54967183-dc1d-452a-9259-2f1e1b0f0634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aac59a3f-2b82-43db-9f1d-b7c2af7e1fa0",
                    "LayerId": "a0dd1581-e4fd-45d3-ab47-19c1a0d8d146"
                }
            ]
        },
        {
            "id": "6972da81-83db-4c73-beb3-4890bb058291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f935e22a-9d19-4c97-8775-9d033c689db5",
            "compositeImage": {
                "id": "7b141fbb-3d20-497c-9000-15b2acaf61ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6972da81-83db-4c73-beb3-4890bb058291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93894b40-fab3-49bf-b051-66deb4e28510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6972da81-83db-4c73-beb3-4890bb058291",
                    "LayerId": "a0dd1581-e4fd-45d3-ab47-19c1a0d8d146"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "a0dd1581-e4fd-45d3-ab47-19c1a0d8d146",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f935e22a-9d19-4c97-8775-9d033c689db5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 0
}
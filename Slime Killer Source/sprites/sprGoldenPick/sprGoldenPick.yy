{
    "id": "7f1dfa64-eb4c-46cf-b216-f99d605cc30a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGoldenPick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "605457b0-7af9-4ffc-b3b0-e9d789818a98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f1dfa64-eb4c-46cf-b216-f99d605cc30a",
            "compositeImage": {
                "id": "28c7f380-e28f-4c4d-afbc-1e652fff65f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "605457b0-7af9-4ffc-b3b0-e9d789818a98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac3fb52e-739d-4ce8-a731-478427a3c1c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "605457b0-7af9-4ffc-b3b0-e9d789818a98",
                    "LayerId": "240aae73-72f3-4d44-8c02-86ea08633868"
                }
            ]
        },
        {
            "id": "5119a489-0fc4-47aa-88d1-1ad99ffd5cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f1dfa64-eb4c-46cf-b216-f99d605cc30a",
            "compositeImage": {
                "id": "130285af-6977-4969-b808-186ff7f2bcc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5119a489-0fc4-47aa-88d1-1ad99ffd5cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f24aa7e-328b-4513-b889-151dd9d44707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5119a489-0fc4-47aa-88d1-1ad99ffd5cca",
                    "LayerId": "240aae73-72f3-4d44-8c02-86ea08633868"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "240aae73-72f3-4d44-8c02-86ea08633868",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f1dfa64-eb4c-46cf-b216-f99d605cc30a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}
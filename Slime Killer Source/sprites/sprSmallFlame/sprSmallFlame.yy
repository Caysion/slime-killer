{
    "id": "977df634-e2de-4f8a-a109-7fb378a05841",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSmallFlame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f38a73a3-00c2-4055-b790-d218d5141f92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "977df634-e2de-4f8a-a109-7fb378a05841",
            "compositeImage": {
                "id": "7aa3948d-ea40-4c79-a1fa-b5cfd5595e54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f38a73a3-00c2-4055-b790-d218d5141f92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4a072b7-9d51-40b2-99a5-845f85eb780e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f38a73a3-00c2-4055-b790-d218d5141f92",
                    "LayerId": "ba123ae6-009c-474a-90f3-858576977942"
                }
            ]
        },
        {
            "id": "b2eda3bb-e2ea-4b09-8a5d-a770914b2159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "977df634-e2de-4f8a-a109-7fb378a05841",
            "compositeImage": {
                "id": "01a4ce80-f60a-45ee-b788-27ddfc86cf75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2eda3bb-e2ea-4b09-8a5d-a770914b2159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72a5b3f4-a00e-4197-9dc3-1ff86934897c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2eda3bb-e2ea-4b09-8a5d-a770914b2159",
                    "LayerId": "ba123ae6-009c-474a-90f3-858576977942"
                }
            ]
        },
        {
            "id": "1dc7c57c-a569-463c-a23a-7e88c0be6747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "977df634-e2de-4f8a-a109-7fb378a05841",
            "compositeImage": {
                "id": "f9813671-368e-4b5e-90dd-858e15ebb87a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dc7c57c-a569-463c-a23a-7e88c0be6747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcc10d60-9878-4d1a-9368-d2056df2306c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dc7c57c-a569-463c-a23a-7e88c0be6747",
                    "LayerId": "ba123ae6-009c-474a-90f3-858576977942"
                }
            ]
        },
        {
            "id": "06750855-4bed-4375-8726-3af0654b49aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "977df634-e2de-4f8a-a109-7fb378a05841",
            "compositeImage": {
                "id": "173bf03d-5da2-4ba6-8300-e30df76d119f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06750855-4bed-4375-8726-3af0654b49aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "855435b9-45d5-4e03-89fb-0bc8cfab6942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06750855-4bed-4375-8726-3af0654b49aa",
                    "LayerId": "ba123ae6-009c-474a-90f3-858576977942"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "ba123ae6-009c-474a-90f3-858576977942",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "977df634-e2de-4f8a-a109-7fb378a05841",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 0
}
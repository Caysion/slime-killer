{
    "id": "cce85c4f-0b90-4145-b17a-85d4db32c348",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHeroFirst",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "273fdc17-722f-4168-bf39-d731bf2b4f0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cce85c4f-0b90-4145-b17a-85d4db32c348",
            "compositeImage": {
                "id": "62345ec2-e47b-4e2c-bbb2-b02054978f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "273fdc17-722f-4168-bf39-d731bf2b4f0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f3788f6-42bd-4d09-90b6-b7fb76b1802c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "273fdc17-722f-4168-bf39-d731bf2b4f0c",
                    "LayerId": "9fab0799-26cd-4a7d-8dd0-61cbbcdc6337"
                }
            ]
        },
        {
            "id": "cba51a57-597d-41f7-8a3a-b7423b16d274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cce85c4f-0b90-4145-b17a-85d4db32c348",
            "compositeImage": {
                "id": "4d405204-b242-4f13-a545-cb9e4e4f6cd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cba51a57-597d-41f7-8a3a-b7423b16d274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75fd8a38-6ffc-4b68-857b-415fa6d96170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cba51a57-597d-41f7-8a3a-b7423b16d274",
                    "LayerId": "9fab0799-26cd-4a7d-8dd0-61cbbcdc6337"
                }
            ]
        },
        {
            "id": "d449da1f-bbc0-4abb-8cdc-37868764f2a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cce85c4f-0b90-4145-b17a-85d4db32c348",
            "compositeImage": {
                "id": "21897e93-e776-42b7-9dfe-f1172573488f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d449da1f-bbc0-4abb-8cdc-37868764f2a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9672442e-2e22-4d9a-894e-bb8f2f1b4fe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d449da1f-bbc0-4abb-8cdc-37868764f2a0",
                    "LayerId": "9fab0799-26cd-4a7d-8dd0-61cbbcdc6337"
                }
            ]
        },
        {
            "id": "23404513-3d87-4f5e-af92-f69d09307362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cce85c4f-0b90-4145-b17a-85d4db32c348",
            "compositeImage": {
                "id": "8646f628-3de3-421a-a96e-2f772401bbb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23404513-3d87-4f5e-af92-f69d09307362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fa55a66-a0cb-4cd8-9abb-04f004eab7e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23404513-3d87-4f5e-af92-f69d09307362",
                    "LayerId": "9fab0799-26cd-4a7d-8dd0-61cbbcdc6337"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "9fab0799-26cd-4a7d-8dd0-61cbbcdc6337",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cce85c4f-0b90-4145-b17a-85d4db32c348",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 12,
    "yorig": 15
}
{
    "id": "99f772aa-aaf6-4a90-a60f-2c754082c87c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86093710-9f84-460c-a1cf-a2bb6cd86667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99f772aa-aaf6-4a90-a60f-2c754082c87c",
            "compositeImage": {
                "id": "c994e4a3-978a-4170-8428-aa83e3c72e0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86093710-9f84-460c-a1cf-a2bb6cd86667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "311fb3c5-5584-44df-a7fa-017312a01d99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86093710-9f84-460c-a1cf-a2bb6cd86667",
                    "LayerId": "240d4254-1c36-42ab-b94b-7d4f1122a5a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "240d4254-1c36-42ab-b94b-7d4f1122a5a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99f772aa-aaf6-4a90-a60f-2c754082c87c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
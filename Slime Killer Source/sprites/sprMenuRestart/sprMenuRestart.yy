{
    "id": "4911f5c3-0eec-41bf-96dc-4646149dc9e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenuRestart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81ca3189-e89b-4f9f-9a73-125fc54375ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4911f5c3-0eec-41bf-96dc-4646149dc9e0",
            "compositeImage": {
                "id": "f107a6cd-0f74-42c5-9913-eaeb6f27df33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81ca3189-e89b-4f9f-9a73-125fc54375ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d544d55-4154-4749-81f0-0e6ff948e3e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81ca3189-e89b-4f9f-9a73-125fc54375ed",
                    "LayerId": "e9c21ad6-28b6-42d6-9626-789a1ce30f6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e9c21ad6-28b6-42d6-9626-789a1ce30f6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4911f5c3-0eec-41bf-96dc-4646149dc9e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}
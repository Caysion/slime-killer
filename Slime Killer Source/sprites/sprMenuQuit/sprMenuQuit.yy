{
    "id": "a514498e-3e9f-4061-8736-e1de7a15a059",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenuQuit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca43481e-2c18-4c15-8742-998e3e3ad8d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a514498e-3e9f-4061-8736-e1de7a15a059",
            "compositeImage": {
                "id": "6b6acc63-e680-4acb-86b5-87d9dd9d3126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca43481e-2c18-4c15-8742-998e3e3ad8d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b9577c1-7971-42d2-85e3-ea0af5dcafc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca43481e-2c18-4c15-8742-998e3e3ad8d8",
                    "LayerId": "5d9c9ae8-b1d5-4401-8c1f-53096034eb46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5d9c9ae8-b1d5-4401-8c1f-53096034eb46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a514498e-3e9f-4061-8736-e1de7a15a059",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}
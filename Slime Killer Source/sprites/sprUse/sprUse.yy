{
    "id": "57abff26-a909-4dde-b2ca-08cee48788ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprUse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "356b5936-cd08-4a97-b91c-879789153b49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57abff26-a909-4dde-b2ca-08cee48788ba",
            "compositeImage": {
                "id": "2bfbb134-9832-4afa-af92-981444e40825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "356b5936-cd08-4a97-b91c-879789153b49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee0b72d-8c17-4d2c-9df8-182d66c0db17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "356b5936-cd08-4a97-b91c-879789153b49",
                    "LayerId": "b0704c55-3202-4c93-aac1-f5dd1393c3c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b0704c55-3202-4c93-aac1-f5dd1393c3c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57abff26-a909-4dde-b2ca-08cee48788ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 2,
    "bbox_right": 19,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "778a1be0-3934-403e-b5f8-6315a9adc133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "2e8943e7-7d5f-43a0-946f-07ea54a8dcb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "778a1be0-3934-403e-b5f8-6315a9adc133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4f11d4f-cefd-43af-bdd2-adde09f02ca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "778a1be0-3934-403e-b5f8-6315a9adc133",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "011acae6-1bd9-4dfd-a027-5046508ba681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "85960980-5564-4867-9d61-99579ac82f0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "011acae6-1bd9-4dfd-a027-5046508ba681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bd26a6f-4c4c-45c2-9d6a-e1ee1ed1195e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "011acae6-1bd9-4dfd-a027-5046508ba681",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "46a54285-5029-4638-8a47-849f6da94dc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "bbed3ade-253f-4db1-8624-346084cba826",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46a54285-5029-4638-8a47-849f6da94dc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae61a354-d45e-4a3c-8be3-649d14c17681",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46a54285-5029-4638-8a47-849f6da94dc3",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "9ef9e3c9-e965-4cc1-9bff-3a9f083857c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "e67e0a41-c6f1-4ebd-98e7-2c6adbca4a84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ef9e3c9-e965-4cc1-9bff-3a9f083857c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccdaef36-566d-4b2a-9075-dc1f55631bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef9e3c9-e965-4cc1-9bff-3a9f083857c8",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "5de6df75-6b4d-4069-acd9-275f00dcbc5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "5ca557ce-61a0-4c8e-8651-ef372cd28b0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de6df75-6b4d-4069-acd9-275f00dcbc5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea5daa15-fdc8-45e6-bf01-b81be9e69798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de6df75-6b4d-4069-acd9-275f00dcbc5c",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "3586f7d7-dd4d-4d96-adc9-d4c7c3e3bc43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "643722cc-5817-4cab-9143-dc3aa2961e7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3586f7d7-dd4d-4d96-adc9-d4c7c3e3bc43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c913d758-1469-409b-9ee7-052fff595950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3586f7d7-dd4d-4d96-adc9-d4c7c3e3bc43",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "a3e853f2-9d35-4639-ab6d-104cf50bf61d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "9b84273c-28c2-4644-a200-141150fa44e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3e853f2-9d35-4639-ab6d-104cf50bf61d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "820a9ab9-840f-4aac-910d-46153dee7572",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3e853f2-9d35-4639-ab6d-104cf50bf61d",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "e96973bc-821e-4348-897b-924609278cfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "680a6a6e-9c37-4a84-931c-f09b7ea6ba25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e96973bc-821e-4348-897b-924609278cfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff5b095-acc2-4516-84cc-4e1b863ab6d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e96973bc-821e-4348-897b-924609278cfd",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "4d6bbcfb-aedf-4ed7-b6b5-b1ad677316b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "176e339f-921c-45bb-b24f-79c139328d07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d6bbcfb-aedf-4ed7-b6b5-b1ad677316b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf565b4c-4719-41fb-833b-1ede1c8097f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d6bbcfb-aedf-4ed7-b6b5-b1ad677316b8",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "30666846-3edb-461b-8ac6-fd395061f12f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "981336ea-7800-4608-a667-ab9d4eb3726b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30666846-3edb-461b-8ac6-fd395061f12f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a10a02a3-65c7-4946-a850-8af03c89aff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30666846-3edb-461b-8ac6-fd395061f12f",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "152697fb-7db7-470e-bcc3-10dd171d3ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "1338546d-459f-4a61-a0f3-bb31053730f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "152697fb-7db7-470e-bcc3-10dd171d3ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93f1e8bd-cfc1-439d-8d34-f2adc67b9594",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "152697fb-7db7-470e-bcc3-10dd171d3ce3",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "63e975c5-5cfd-47a8-9ebe-7d2aec52c951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "745ff3b4-7f2c-4ab4-be2c-009830128d2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63e975c5-5cfd-47a8-9ebe-7d2aec52c951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e375bd7-e3ae-4d5c-839a-aaf7b72da14c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63e975c5-5cfd-47a8-9ebe-7d2aec52c951",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "ef8a75f6-935a-4af2-918b-ca879e216d26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "690984bf-199c-40c9-9810-626c8c097f62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef8a75f6-935a-4af2-918b-ca879e216d26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482b0544-c5dc-4010-b1f9-c272f9f2a986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef8a75f6-935a-4af2-918b-ca879e216d26",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "f409a913-1bb5-4eba-81ec-6c04937c4155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "bfc9631e-056d-464f-abfc-65ee8c78fc6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f409a913-1bb5-4eba-81ec-6c04937c4155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b131d51a-4a62-4dbe-b8ef-9f825c89e0c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f409a913-1bb5-4eba-81ec-6c04937c4155",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "4ca1a416-76ce-4eb7-baf9-a5763251c56a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "220e6fd1-bd4f-41ba-b9c1-6483348b6615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ca1a416-76ce-4eb7-baf9-a5763251c56a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0a733dd-6c73-4b3d-9d34-09ea22d12b0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ca1a416-76ce-4eb7-baf9-a5763251c56a",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        },
        {
            "id": "e4a82a27-f0a5-4518-9173-d5ca924b3b8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "compositeImage": {
                "id": "d10e7f92-4dc1-4333-8de7-f15eb2a48713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a82a27-f0a5-4518-9173-d5ca924b3b8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73e9a5c1-3f14-47a6-ad26-a803af2f6d49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a82a27-f0a5-4518-9173-d5ca924b3b8b",
                    "LayerId": "9408bbca-5447-4dd4-a53a-4828c7c36e39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "9408bbca-5447-4dd4-a53a-4828c7c36e39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9af20faf-6e4c-4a0c-93e0-1e5382baacbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 14
}
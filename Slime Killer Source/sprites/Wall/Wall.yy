{
    "id": "071e924d-9418-4a6b-ab70-991bc996fe95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cac1311-7186-4823-b7ad-0dd1a9a96406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "071e924d-9418-4a6b-ab70-991bc996fe95",
            "compositeImage": {
                "id": "b9639ff3-0501-4af4-b2f5-cf30595b551f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cac1311-7186-4823-b7ad-0dd1a9a96406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23431c31-5a05-4d07-963c-24db15e735c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cac1311-7186-4823-b7ad-0dd1a9a96406",
                    "LayerId": "f75f6f98-913b-49c3-aa4e-82d0319da39f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f75f6f98-913b-49c3-aa4e-82d0319da39f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "071e924d-9418-4a6b-ab70-991bc996fe95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
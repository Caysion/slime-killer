{
    "id": "e4f6323a-2e9f-4160-82a3-918cca77eb36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBigSkeleton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c52e7b24-79e3-449d-bc0d-68a3e495dd10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4f6323a-2e9f-4160-82a3-918cca77eb36",
            "compositeImage": {
                "id": "8f650549-3f05-4eb5-9b24-1e5e10c49a01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c52e7b24-79e3-449d-bc0d-68a3e495dd10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10cb7229-c41a-4d31-898f-67325106932a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c52e7b24-79e3-449d-bc0d-68a3e495dd10",
                    "LayerId": "f846873c-fe74-4f31-bba0-1f946c007e1e"
                }
            ]
        },
        {
            "id": "a0b396cc-9708-4e2a-b89c-ba2d9b154d97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4f6323a-2e9f-4160-82a3-918cca77eb36",
            "compositeImage": {
                "id": "288f2944-c994-45b6-80bf-d4053f83f996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0b396cc-9708-4e2a-b89c-ba2d9b154d97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "787e6070-f6a2-48c4-89a6-bdd4b1aab9aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0b396cc-9708-4e2a-b89c-ba2d9b154d97",
                    "LayerId": "f846873c-fe74-4f31-bba0-1f946c007e1e"
                }
            ]
        },
        {
            "id": "408ec7bb-eb5c-4ec8-8d8f-04471ac52829",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4f6323a-2e9f-4160-82a3-918cca77eb36",
            "compositeImage": {
                "id": "32a00996-4c2f-446d-99c1-a66f8a8d9065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "408ec7bb-eb5c-4ec8-8d8f-04471ac52829",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80f99e17-2d60-4904-ba8a-a24aa6430589",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "408ec7bb-eb5c-4ec8-8d8f-04471ac52829",
                    "LayerId": "f846873c-fe74-4f31-bba0-1f946c007e1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f846873c-fe74-4f31-bba0-1f946c007e1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4f6323a-2e9f-4160-82a3-918cca77eb36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 8
}
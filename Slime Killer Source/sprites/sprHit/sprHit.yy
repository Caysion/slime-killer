{
    "id": "e39dec85-49dd-4c24-8bef-9e515ec8e0a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51129353-4d5e-46a0-83e0-915eb9b323b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e39dec85-49dd-4c24-8bef-9e515ec8e0a5",
            "compositeImage": {
                "id": "b17d48f0-025f-48b1-81d4-0831d96046a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51129353-4d5e-46a0-83e0-915eb9b323b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c6e45c2-d256-40ef-b747-94df4760163f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51129353-4d5e-46a0-83e0-915eb9b323b6",
                    "LayerId": "8227572f-1350-4762-b07a-01a54847f524"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8227572f-1350-4762-b07a-01a54847f524",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e39dec85-49dd-4c24-8bef-9e515ec8e0a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "3bc8971d-e572-44d2-8614-a13de13533ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStaminaPotion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe8b6354-5009-4cd3-8efb-3267c1602277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bc8971d-e572-44d2-8614-a13de13533ab",
            "compositeImage": {
                "id": "814c2ae4-3dbf-4c03-910a-e38b2e205645",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe8b6354-5009-4cd3-8efb-3267c1602277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e47ea7b4-fbce-4c04-916e-05645bc85b78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe8b6354-5009-4cd3-8efb-3267c1602277",
                    "LayerId": "14ba45e4-48d2-494f-9640-3325c4263663"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "14ba45e4-48d2-494f-9640-3325c4263663",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bc8971d-e572-44d2-8614-a13de13533ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
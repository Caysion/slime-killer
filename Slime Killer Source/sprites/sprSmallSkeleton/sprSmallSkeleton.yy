{
    "id": "3bf38c14-d48e-4686-bc17-8dc85b6b8672",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSmallSkeleton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5eda3ab-3267-4428-b0c8-ec35c5275ccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf38c14-d48e-4686-bc17-8dc85b6b8672",
            "compositeImage": {
                "id": "413984bb-1c87-4a8d-8040-11801c4a7dfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5eda3ab-3267-4428-b0c8-ec35c5275ccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05d9fdcb-29e5-453b-a176-5f682e8ecb09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5eda3ab-3267-4428-b0c8-ec35c5275ccb",
                    "LayerId": "0c32c743-d3a0-4f76-a7e7-ededcc54c48a"
                }
            ]
        },
        {
            "id": "027883d1-2a30-4320-b6db-f6fbe87b9bc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf38c14-d48e-4686-bc17-8dc85b6b8672",
            "compositeImage": {
                "id": "10ad4c89-7e57-42ba-a4c4-4eea976a1cd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "027883d1-2a30-4320-b6db-f6fbe87b9bc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "835a2d87-0cd8-454b-8df9-f6c37b35a93d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "027883d1-2a30-4320-b6db-f6fbe87b9bc2",
                    "LayerId": "0c32c743-d3a0-4f76-a7e7-ededcc54c48a"
                }
            ]
        },
        {
            "id": "059f56bd-0131-4b06-b7b7-af8e61be843f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf38c14-d48e-4686-bc17-8dc85b6b8672",
            "compositeImage": {
                "id": "b555f39d-570a-4ebd-b0ff-1f117e3dea00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "059f56bd-0131-4b06-b7b7-af8e61be843f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77358779-838b-414f-b054-b5e4a69a4ef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "059f56bd-0131-4b06-b7b7-af8e61be843f",
                    "LayerId": "0c32c743-d3a0-4f76-a7e7-ededcc54c48a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "0c32c743-d3a0-4f76-a7e7-ededcc54c48a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bf38c14-d48e-4686-bc17-8dc85b6b8672",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 0
}
{
    "id": "91031c02-9923-4a42-b5cc-5bc810685a55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGoldenSwPick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fb7a41a-b666-480a-89bd-0fe22ee2016a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91031c02-9923-4a42-b5cc-5bc810685a55",
            "compositeImage": {
                "id": "2896e84d-f9da-4efd-b8bd-5090406bdf30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fb7a41a-b666-480a-89bd-0fe22ee2016a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "212f98f7-aabd-4230-b4db-b4374201bc11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fb7a41a-b666-480a-89bd-0fe22ee2016a",
                    "LayerId": "4f106e94-0cf1-46a7-b311-a537290ee630"
                }
            ]
        },
        {
            "id": "bb8dd4ea-af57-44f6-9680-2b01992689f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91031c02-9923-4a42-b5cc-5bc810685a55",
            "compositeImage": {
                "id": "c59d08d3-790c-42f0-9e8f-a99806936ba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb8dd4ea-af57-44f6-9680-2b01992689f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fbd5bcf-0bdc-401a-a864-295f1d91c559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb8dd4ea-af57-44f6-9680-2b01992689f3",
                    "LayerId": "4f106e94-0cf1-46a7-b311-a537290ee630"
                }
            ]
        },
        {
            "id": "bfe7164f-1e9f-40ff-a42c-7c030759b530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91031c02-9923-4a42-b5cc-5bc810685a55",
            "compositeImage": {
                "id": "09593934-55ad-4c49-bdad-1673d3db6165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfe7164f-1e9f-40ff-a42c-7c030759b530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef16bba7-2344-4ac0-b57f-49a2901ea2e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfe7164f-1e9f-40ff-a42c-7c030759b530",
                    "LayerId": "4f106e94-0cf1-46a7-b311-a537290ee630"
                }
            ]
        },
        {
            "id": "130d7107-86e8-46ec-a9cd-7e1bc00f83f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91031c02-9923-4a42-b5cc-5bc810685a55",
            "compositeImage": {
                "id": "fe77a03e-4c35-4fca-aae4-d59abde7ea29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "130d7107-86e8-46ec-a9cd-7e1bc00f83f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18f0fc01-d69c-4187-bb02-c6673e54cdfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "130d7107-86e8-46ec-a9cd-7e1bc00f83f1",
                    "LayerId": "4f106e94-0cf1-46a7-b311-a537290ee630"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "4f106e94-0cf1-46a7-b311-a537290ee630",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91031c02-9923-4a42-b5cc-5bc810685a55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}
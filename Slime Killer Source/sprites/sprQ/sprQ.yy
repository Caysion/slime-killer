{
    "id": "c986e1ce-e973-496d-bd3e-368a5401adba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprQ",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6ee4ffb-edd3-4a00-9544-7ddcb2b86ad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c986e1ce-e973-496d-bd3e-368a5401adba",
            "compositeImage": {
                "id": "7c29180f-0e67-46da-852b-cf04ad19b5c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6ee4ffb-edd3-4a00-9544-7ddcb2b86ad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f70c7de3-4b04-420c-bef3-bd2f77d97b14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6ee4ffb-edd3-4a00-9544-7ddcb2b86ad8",
                    "LayerId": "7d49f022-8113-48a9-92ed-ad7e1d199fdc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7d49f022-8113-48a9-92ed-ad7e1d199fdc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c986e1ce-e973-496d-bd3e-368a5401adba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
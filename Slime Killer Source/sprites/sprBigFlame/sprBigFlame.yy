{
    "id": "7934d688-c1b8-4042-a41c-39ddf7f135c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBigFlame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ba5ed77-c11f-40a0-8bad-36c7d951c36c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7934d688-c1b8-4042-a41c-39ddf7f135c1",
            "compositeImage": {
                "id": "76f3ec35-5829-4bfe-a891-d013753c498b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ba5ed77-c11f-40a0-8bad-36c7d951c36c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "849731cd-cc34-4078-97c3-3975b9bd8c44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ba5ed77-c11f-40a0-8bad-36c7d951c36c",
                    "LayerId": "46eaabd8-7ccd-433e-ac83-3e7ece06854e"
                }
            ]
        },
        {
            "id": "9c2f373e-73ca-40d9-bc71-12d6e09b969e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7934d688-c1b8-4042-a41c-39ddf7f135c1",
            "compositeImage": {
                "id": "c7f673d7-1b1b-4ff2-b6ab-39604e0c5cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c2f373e-73ca-40d9-bc71-12d6e09b969e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1019984-2f15-4696-aa11-c7fb4682de2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c2f373e-73ca-40d9-bc71-12d6e09b969e",
                    "LayerId": "46eaabd8-7ccd-433e-ac83-3e7ece06854e"
                }
            ]
        },
        {
            "id": "84ab5fb8-4de1-4ca1-bb78-c5350f4895d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7934d688-c1b8-4042-a41c-39ddf7f135c1",
            "compositeImage": {
                "id": "ec74b421-d1e4-4cc5-a9a9-2d77478b856f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84ab5fb8-4de1-4ca1-bb78-c5350f4895d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab2ba90a-8279-4d2a-83cb-5dae237a5048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84ab5fb8-4de1-4ca1-bb78-c5350f4895d4",
                    "LayerId": "46eaabd8-7ccd-433e-ac83-3e7ece06854e"
                }
            ]
        },
        {
            "id": "40f62c0b-50ad-416a-8f84-9c84b3ee456d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7934d688-c1b8-4042-a41c-39ddf7f135c1",
            "compositeImage": {
                "id": "2c3e6cb8-6190-4e2e-bd9b-87eb8e0460c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40f62c0b-50ad-416a-8f84-9c84b3ee456d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cfdf711-372e-47c3-8f85-8bda21b0bc8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40f62c0b-50ad-416a-8f84-9c84b3ee456d",
                    "LayerId": "46eaabd8-7ccd-433e-ac83-3e7ece06854e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "46eaabd8-7ccd-433e-ac83-3e7ece06854e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7934d688-c1b8-4042-a41c-39ddf7f135c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 0
}